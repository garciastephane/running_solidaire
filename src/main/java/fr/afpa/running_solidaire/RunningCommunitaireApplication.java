package fr.afpa.running_solidaire;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RunningCommunitaireApplication {

	public static void main(String[] args) {
		SpringApplication.run(RunningCommunitaireApplication.class, args);
	}

}
