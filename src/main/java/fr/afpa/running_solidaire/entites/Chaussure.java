package fr.afpa.running_solidaire.entites;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Chaussure {

	private Integer id;
	private EtatChaussure etat;
	private int quantite;
	
	public Chaussure() {
		super();
		this.etat = etat;
		this.quantite = quantite;
	}
}
