package fr.afpa.running_solidaire.entites;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode


public  class Personne implements Serializable{
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 559286937448398592L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="native")
	@Column
	private Long id;
	@Column
	private String nom;
	@Column
	private String prenom;
	@Column
	private LocalDate dateNaissance;
	@Column
	private String email;
	@Column
	private String adresse;
	@Column
	private String username;
	@Column
	private String password;
	@Transient
	private String confirmPassword;
	private RolePersonne role;

	public Personne() {
		super();
	}

	public Personne( Long id, String nom, String prenom, LocalDate dateNaissance, String email, String adresse, String username, String password,
			String confirmPassword,RolePersonne role) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.email = email;
		this.adresse = adresse;
		this.username = username;
		this.password = password;
		this.confirmPassword = confirmPassword;
		this.role = role;
	}

}

