package fr.afpa.running_solidaire.entites;

public enum EtatChaussure {
	
	BON(1,"bon"), MOYEN(2, "moyen"), MAUVAIS(3, "mauvais");
	private Integer id;
	private String etat;
	
	EtatChaussure(Integer id, String etat) {
		this.id = id;
		this.etat = etat;
	}


}
