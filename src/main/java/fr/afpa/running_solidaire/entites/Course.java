package fr.afpa.running_solidaire.entites;

import java.util.ArrayList;
import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode

public class Course {
	private Integer id;
	private String nom;
	private String typeCourse;
	private List<Evenement> listeEvenements;
	
	public Course() {
		super();
	}
	
	public Course(String nom) {
		super();
		this.nom = nom;
		this.listeEvenements = new ArrayList();
	}
	
	public Course(String nom, List<Evenement> listeEvenements) {
		super();
		this.nom = nom;
		this.listeEvenements = listeEvenements;
	}

}
