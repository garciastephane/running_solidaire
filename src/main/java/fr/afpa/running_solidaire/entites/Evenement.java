package fr.afpa.running_solidaire.entites;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Evenement {
	private Integer id;
	private String nom;
	private LocalDate dateDebut;
	private LocalDate dateFin;
	private String lieu;
	private ProfilEvenement profilEvenement;
	private List<Inscription> listeInscriptions;
	private List<CollecteChaussure> listCollecteChaussures;

	public Evenement() {
		super();
	}

	public Evenement(String nom, LocalDate dateDebut, LocalDate dateFin) {
		super();
		this.nom = nom;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.lieu = lieu;

		this.listeInscriptions = new ArrayList<Inscription>();
		this.listCollecteChaussures = new ArrayList<CollecteChaussure>();
	}

}
