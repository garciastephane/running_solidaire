package fr.afpa.running_solidaire.entites;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode

public class CreerUser {

	private String nom;
	private String prenom;
	private String dateNaissance;
	private String mail;
	private String adresse;
	private String actif;
	private String role;
	private String login;
	private String password;
	private String create;
}
