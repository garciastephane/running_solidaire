package fr.afpa.running_solidaire.entites;

import java.util.ArrayList;
import java.util.List;

public class CollecteChaussure {
	private float pointure;
	private String marque;
	private char sexe;
	private String couleur;
	private List<Chaussure> listeChaussures;
	private List<Inscription> listeInscriptions;

	public CollecteChaussure() {
		super();
	}

	public CollecteChaussure(float pointure, String marque, char sexe, String couleur) {

		super();
		{
			this.pointure = pointure;
			this.marque = marque;
			this.sexe = sexe;
			this.couleur = couleur;
			this.listeChaussures = new ArrayList<Chaussure>();
			this.listeInscriptions = new ArrayList<Inscription>();
		}

	}
}