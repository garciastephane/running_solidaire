package fr.afpa.running_solidaire.entites;

import java.time.LocalDate;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
public class Administrateur extends Personne {

	public Administrateur() {
		super();
	}

	public Administrateur(Long id, String nom, String prenom, LocalDate dateNaissance, String email, String adresse, String username, String password,
			String confirmPassword,RolePersonne role) {
		super(id,nom, prenom, dateNaissance, email, adresse, username,password,confirmPassword, role);
	}

}