package fr.afpa.running_solidaire.entites;

public enum ProfilEvenement {

	MARATHON(1, "marathon"), SEMIMARATHON(2, "semimarathon"), SPRINT(3, "sprint"), TRAIL(4, "trail");

	private Integer id;
	private String profil;

	ProfilEvenement(Integer id, String profil) {
		this.id = id;
		this.profil = profil;
	}

}
