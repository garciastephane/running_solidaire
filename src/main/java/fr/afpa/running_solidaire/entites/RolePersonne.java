package fr.afpa.running_solidaire.entites;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@ToString

@Entity
public class RolePersonne implements Serializable {

	/*
	 * private Long id; private String name; private String description
	 * 
	 * RolePersonne(Long id, String name, String description ) { this.id = id;
	 * this.name = name; this.description =description; }
	 */

	/**
	 * 
	 */
	private static final long serialVersionUID = -4241571053356806622L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")

	private Long id;

	@Column
	private String name;

	@Column
	private String description;

}
